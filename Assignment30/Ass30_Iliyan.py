from jnpr.junos import Device
from lxml import etree
from jnpr.junos.utils.config import Config

hostIP = '192.168.2.1'
user = 'root'
password = 'Juniper1'
file = 'ConfigFile.conf'
ConfigPath = '/home/iliyan/Assignments/'

dev = Device(host = hostIP, user = user, password = password)
print("Opening connection to", hostIP)
dev.open()
MyConfig = dev.rpc.get_config(options={'format':'text'})
TextConfig = etree.tostring(MyConfig, encoding = 'unicode', pretty_print = True)
TextConfig = '\n'.join(TextConfig.split('\n')[1:-2])

ConfigFile = input('Enter config file name: ')
if not ConfigFile: ConfigFile = file
ConfigFilePath = ConfigPath + ConfigFile

ConfigFile = open(ConfigFilePath, 'w')
ConfigFile.write(TextConfig)
ConfigFile.close()

print('The config file is in the ' + ConfigFilePath + ' file')

input('Change the config file and press Enter. ')

print('Changed successfully ')
ConfigTheFile = open(ConfigFilePath, 'r')
FileContent = ConfigTheFile.read()
ConfigTheFile.close()
print(FileContent)

dev.bind(cu = Config)
dev.cu.lock()
dev.cu.load(path=ConfigFilePath, overwrite = True)
dev.cu.commit()
dev.cu.unlock()

print('DONE :)')

print('Verifuing the commit...')
print(dev.cli('show system commit', warning = False))
dev.close()



