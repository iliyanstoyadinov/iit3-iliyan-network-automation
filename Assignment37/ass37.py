import os
import netmiko

Router1Connection = netmiko.ConnectionHandler(ip = '192.168.2.1',
										device_type = 'juniper',
										username = 'root',
										password = 'Juniper1')

Router1Connection.config_mode()
with open('Router1Conf.json', 'w') as f:
		f.write(Router1Connection.send_command("show"))

print(Router1Connection.send_config_from_file("srxConf.set",
												exit_config_mode = False))
print(Router1Connection.commit(and_quit = True))

Router1Connection.disconnect()

Router2Connection = netmiko.ConnectionHandler(ip = '192.168.10.2',
										device_type = 'juniper',
										username = 'root',
										password = 'Juniper1')

Router2Connection.config_mode()
with open('Router2Conf.json', 'w') as f:
		f.write(Router2Connection.send_command("show"))

print(Router2Connection.send_config_from_file("srxConf.set",
												exit_config_mode = False))
print(Router2Connection.commit(and_quit = True))

Router2Connection.disconnect()

